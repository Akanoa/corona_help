import { shallowMount } from "@vue/test-utils";
import CustomInput from "../../src/components/CustomInput";

describe("HelloWorld.vue", () => {
  it("renders props.msg when passed", () => {
    const value = "new message";
    const wrapper = shallowMount(CustomInput, {
      propsData: { value }
    });
    expect(wrapper.name()).toBe('CustomInput');
  });
});
