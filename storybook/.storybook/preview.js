import { addParameters } from "@storybook/vue";

addParameters({
    backgrounds: [
        {name: 'background', value:'#1F1F22', default: true},
        {name: 'foreground1', value:'#28292C'},
        {name: 'foreground2', value:'#393D47'},
        {name: 'tertiary', value:'#2F3136'},
    ]
})