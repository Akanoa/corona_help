import CustomInput from '../src/components/CustomInput';
import {boolean, text, select, withKnobs} from '@storybook/addon-knobs';
import {withA11y} from '@storybook/addon-a11y';
import {action} from '@storybook/addon-actions';

export default {
    title: 'Form/Input',
    decorators: [withKnobs({
        escapeHTML:false
    }), withA11y],
    component: CustomInput
};

let inputTypesValues = {
    "Texte": 'text',
    "Mot de passe": 'password',
    "Adresse mail": 'email',
    "URL": 'url',
    "Numéro de téléphone": 'tel'
};

export const Input = () => ({
    components: {CustomInput},
    template: `<div>
        <CustomInput
                :placeholder="placeholder" 
                :error="error" 
                :aria="{label:'Nom'}"
                :active="active"
                :type="type"
                v-model="value"
        ></CustomInput>
        </div>`,
    props: {
        placeholder: {
            default: text("Le placeholder", "Nom")
        },
        error: {
            default: text("Le texte d'erreur", "")
        },
        active: {
            default: boolean("Input actif", true)
        },
        type: {
            default: select("Type de l'input", inputTypesValues, 'text')
        }
    },
    data() {
        return {
            value: ""
        }
    },
    methods: {
        click: action('click')
    }
});