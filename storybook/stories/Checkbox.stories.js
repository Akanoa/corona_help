import CustomCheckbox from "../src/components/CustomCheckbox";
import {select, text, boolean, object, withKnobs} from '@storybook/addon-knobs';
import {withA11y} from '@storybook/addon-a11y';
import {action} from '@storybook/addon-actions';
import ToggleSwitch from "../src/components/ToggleSwitch";

export default {
    title: 'Form/Checkbox',
    decorators: [withKnobs({
        escapeHTML:false
    }), withA11y],
    component: CustomCheckbox
};

let selectValues = {
    Orange: "orange",
    Vert: "green",
};

export const Checkbox = () => ({
    components: {CustomCheckbox},
    template: `<CustomCheckbox
            :style="'--width:'+size"
            :type="type"
            :value="true"
            @input="input"
    ></CustomCheckbox>`,
    props: {
        size: {
            default: text("Taille de la checkbox", '10vw')
        },
        type: {
            default: select("Couleur en position gauche", selectValues, 'green')
        },
    },
    methods: {
        input: action('input')
    }
});