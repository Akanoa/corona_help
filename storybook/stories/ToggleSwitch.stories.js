import ToggleSwitch from "../src/components/ToggleSwitch";
import {select, text, boolean, object, withKnobs} from '@storybook/addon-knobs';
import {withA11y} from '@storybook/addon-a11y';
import {action} from '@storybook/addon-actions';

export default {
    title: 'Form/ToggleSwitch',
    decorators: [withKnobs({
        escapeHTML:false
    }), withA11y],
    component: ToggleSwitch
};

let selectValues = {
    Gris: "grey",
    Orange: "orange",
    Vert: "green",
};

export const Switch = () => ({
    components: {ToggleSwitch},
    template: `<ToggleSwitch 
            :left-color="leftColor" 
            :right-color="rightColor" 
            :left-text="leftText"
            :right-text="rightText"
            :style="'--width:'+size"
            :value="value"
            :output-values="outputValues"
            @input="input"></ToggleSwitch>`,
    props: {
        leftColor: {
            default: select("Couleur en position gauche", selectValues, 'green')
        },
        rightColor: {
            default: select("Couleur en position droite", selectValues, 'orange')
        },
        size: {
            default: text("Taille du slider", '10vw')
        },
        leftText: {
            default: text("Texte de gauche", 'Text de gauche')
        },
        rightText: {
            default: text("Texte de droite", 'Text de droite')
        },
        value: {
            default: boolean("Activé", true)
        },
        outputValues: {
            default: object("Valeurs de sortie", {
                right:'right',
                left:'left',
            })
        }
    },
    methods: {
        input: action('input')
    }
});