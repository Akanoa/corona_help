import CustomButton from '../src/components/CustomButton';
import {select, text, withKnobs} from '@storybook/addon-knobs';
import {withA11y} from '@storybook/addon-a11y';
import {action} from '@storybook/addon-actions';

export default {
    title: 'Form/Button',
    decorators: [withKnobs({
        escapeHTML:false
    }), withA11y],
    component: CustomButton
};

let selectValues = {
    Orange: "orange",
    Vert: "green",
    Bleu: "blue"
};

export const Bouton = () => ({
    components: {CustomButton},
    template: `
        <CustomButton :label="label" :type="type" @click="click"></CustomButton>`,
    props: {
        label: {
            default: text('Label du bouton', "Se connecter")
        },
        type: {
            default: select("Type", selectValues, 'blue')
        }
    },
    methods: {
        click: action('click')
    }
});