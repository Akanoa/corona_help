import Category from "../src/components/Category";
import {select, text, boolean, object, withKnobs} from '@storybook/addon-knobs';
import {withA11y} from '@storybook/addon-a11y';
import {action} from '@storybook/addon-actions';

export default {
    title: 'Molecule/Category',
    decorators: [withKnobs({
        escapeHTML:false
    }), withA11y],
    component: Category
};

let selectType = {
    "Garde d'enfants": "child",
    "Aide aux courses": "errants",
    "Garde d'animaux": "pet",
    "Tenir compagnie": "talk"
};

let selectColor = {
    "Orange": "orange",
    "Vert": "green"
};

export const Switch = () => ({
    components: {Category},
    template: `<Category :type="type"
                         :color="color"
                         :style="'--width:'+size"
                         @input="input"
            ></Category>`,
    props: {
        type: {
            default: select("Type de catégorie", selectType, 'pet')
        },
        color: {
            default: select("Couleur du composant", selectColor, 'orange')
        },
        value: {
            default: boolean("Activé", true)
        },
        size: {
            default: text("Taille", '20vw')
        },
    },
    methods: {
        input: action('input')
    }
});