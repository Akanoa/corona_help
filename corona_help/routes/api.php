<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/auth/refresh', 'AuthController@refresh');

Route::get("/markers", 'MarkerController@index');
Route::middleware('auth:api')->post("/marker/add", 'MarkerController@create');
Route::middleware('auth:api')->post("/marker/update", 'MarkerController@update');
Route::middleware('auth:api')->delete("/marker/{uuid}", 'MarkerController@destroy');



Route::middleware('auth:api')->post("/conversation/create", 'ConversationController@create');
Route::middleware('auth:api')->post("/conversation/addMessage", 'ConversationController@addMessage');

Route::get("/sse", 'EventController@sse');

Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {

    Route::post('login', 'AuthController@login')->name('login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::get('me', 'AuthController@me');
    Route::post('register', 'AuthController@register');

});
