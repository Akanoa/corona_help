<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
use Ramsey\Uuid\Uuid;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'uuid' => Uuid::uuid4()->toString(),
        'name' => $faker->firstName,
        'email' => $faker->unique()->safeEmail,
        'verified' => true,
        'password' => '$2y$10$MRlqPoqrAV6Nj2in1gVfL./J7Qji1yNbPaQpObexhqT70mfr9s1vu', // password
        'token' => null
    ];
});
