<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Vérification de l'email</title>
    <style>
        body {
            background-color: #EAF5F4;
            color: #212529;
            padding: 30px;
        }
        .wrapper {
            text-align: center;
            background-color: #CCE5DF;
            padding: 20px;
            width: fit-content;
            margin: auto;
            border-radius: 15px;
        }
    </style>
</head>
<body>
    @yield('content')
</body>
</html>
