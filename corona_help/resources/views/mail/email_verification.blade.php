@extends('layouts.email')

@section('content')
<h1>Bonjour, merci de votre inscription</h1>
<h3>Pour valider votre inscription, veuillez suivre ce lien:</h3>
<div class="wrapper">
    <a href="{{$link}}">{{$link}}</a>
</div>
@endsection
