@extends('layouts.email')

@section('content')
    <h1>Votre compte est validé</h1>
    <p>Merci du temps que vous allez consacrer aux autres 😃</p>
@endsection
