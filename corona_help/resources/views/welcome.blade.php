<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Coronavirus Help</title>
    <link rel="stylesheet" type="text/css" href="css/app.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>

    <div id="mainContainer">
        <div id="headerContainer">
            <div></div>
            <h1 id="title">Solidarité Covid-19</h1>
        </div>
        <div id="contentContainer">
            <div id="mapContainer">

                <div class="notificationBox">
                    <div class="alert alert-danger alert-primary d-none" id="notificationBoxError" role="alert">
                        <h4 class="alert-heading">Erreur</h4>
                        <span class="content"></span>
                    </div>
                    <div class="alert alert-success d-none" id="notificationBoxSuccess" role="alert">
                        <h4 class="alert-heading">Succès</h4>
                        <span class="content"></span>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <script src="js/app.js"></script>
    @if(config('app.env') == 'local')
        <script src="http://localhost:35729/livereload.js"></script>
    @endif
</body>
</html>
