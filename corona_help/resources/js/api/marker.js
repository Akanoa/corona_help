/**
 * Create a marker from data
 * @param data The marker data
 * @returns {Promise<string>}
 */
import {loginGuard} from "../auth";

export function persistMarker(data) {

    return fetch("api/marker/add",{
        method: "post",
        headers: {
            "Content-Type": "application/json"
        },
        body: data
    }).then(function (response) {
        if (!response.ok) {
            throw response
        }
        return response.json()
    })
}


export function updateMarker(data) {

    return loginGuard(() => {

        return fetch("api/marker/update",{
            method: "post",
            headers: {
                "Content-Type": "application/json"
            },
            body: data
        }).then(function (response) {
            if (!response.ok) {
                throw response
            }
            return response.json()
        })
    });
}

/**
 * Remove a specific marker following its UUID
 * @param uuid The marker to remove UUID
 * @returns {Promise<Object>}
 */
export function apiRemoveMarker(uuid) {

    return loginGuard(() => {
        return fetch("api/marker/"+uuid, {
            method: "delete"
        }).then(response => {
            if (!response.ok) {
                throw response;
            }
            return response.json()
        })
    });
}

/**
 * Get all markers
 * @returns {Promise<Object>}
 */
export function loadMarkers() {
    return fetch("api/markers", {
        method: "get"
    }).then(function (response) {
        if (!response.ok) {
            throw response;
        }
        return response.json()
    });
}
