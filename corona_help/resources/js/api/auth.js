/**
 * Register a new user with password and email
 * @param email User email
 * @param password User password
 * @param name User name
 * @returns {Promise<Object>}
 */
export function register({name, email, password, password_verification}) {
    return fetch("api/auth/register",{
        method: "post",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            name,
            email,
            password,
            password_verification
        })
    }).then(function (response) {

        if (!response.ok) {
            throw response
        }

        return response.json()
    })

}

/**
 * Get user details of a connected user
 * @returns {Promise<any>}
 */
export function me() {
    return fetch('/api/auth/me', {
        method: "get"
    }).then(response => {
        if (!response.ok) {
            throw response
        }

        return response.json()
    })
}

/**
 * Log in an existing user
 * @param email User email
 * @param password User password
 * @returns {Promise<Object>}
 */
export function login(email, password) {
    return fetch("api/auth/login", {
        method: "post",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            email,
            password
        })
    }).then(function (response) {

        if (!response.ok) {
            throw response;
        }

        return response.text()
    })
}
