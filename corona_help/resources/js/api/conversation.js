/**
 * Create a conversation bound to a marker
 * @param marker_uuid The marker uuid that init the conversation
 * @param content The first message conversation content
 * @param name The conversation name
 * @returns {Promise<Object>}
 */
import {currentUser, loginGuard} from "../auth";

export function createConversationFromMarker(marker_uuid, content, name) {


    return loginGuard(() => {
        return fetch("api/conversation/create",{
            method: "post",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                user_uuid: currentUser.uuid,
                marker_uuid,
                content,
                name
            })
        }).then(function (response) {
            if (!response.ok) {
                throw response
            }
            return response.json()
        })
    });
}

/**
 * Add a new message from an existing conversation
 * @param uuid
 * @param content
 * @param author
 * @returns {Promise<Object>}
 */
export function addMessageToConversation(uuid, content, author) {

    return fetch("api/conversation/addMessage",{
        method: "post",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            uuid,
            content,
            author
        })
    }).then(function (response) {
        if (!response.ok) {
            throw response
        }
        return response.json()
    })
}
