import L from 'leaflet'
import 'leaflet.markercluster'
import {createMarkerActionPopup} from "./template";
import {loadMarkers, persistMarker} from "./api/marker";
import {openErrorNotification} from "./notification";
import {currentUser, loginGuard} from "./auth";
import {CAN_HELP, NEED_HELP} from "./entities/Marker";
import {publish} from "./utils/eventBus";

class Marker {

    constructor() {
        this.lat = 0;
        this.lng =  0;
        this.type = NEED_HELP;
        this.details =  0
    }

    toObject() {
        return {
            lat: currentMarker.lat,
            lng: currentMarker.lng,
            details: currentMarker.details,
            type: currentMarker.type
        }
    }
}

export const configuration = [
    {
        label : "Je peux aider",
        value: CAN_HELP,
        options: [
            {
                label: "Faire les courses pour une autre personne",
                value: "errants"
            },
            {
                label: "Tenir compagnie à une personne isolée",
                value: "keep_company"
            },
            {
                label: "Garder les enfants des autres",
                value: "keep_children"
            },
            {
                label: "Garder des animaux ou les promener",
                value: "keep_pets"
            }
        ],
        buttonLabel: "Se proposer",
        heading: "Qu'est ce que je peux faire pour les autres ?"
    },
    {
        label : "J'ai besoin d'aide",
        value: NEED_HELP,
        options: [
            {
                label: "Il faut que l'on fasse mes courses",
                value: "errants"
            },
            {
                label: "J'aimerai que l'on me tienne compagnie",
                value: "keep_company"
            },
            {
                label: "Il faudrait que l'on garde mes enfants",
                value: "keep_children"
            },
            {
                label: "Il faudrait que l'on promène ou garde mes animaux",
                value: "keep_pets"
            }
        ],
        buttonLabel: "Demander de l'aide",
        heading: "De quoi ai-je besoin ?"
    }
];

export let currentMarker = new Marker();


export function getClusters() {

    return window.corona.clusters
}

export function createMarkerWithPersistence({lat, lng, type, details}) {

    loginGuard(function () {

        let data = JSON.stringify({
            type,
            lat,
            lng,
            creator_uuid : currentUser.uuid,
            details
        });

        persistMarker(data)
            .then(() => {})
        .finally(() => {
            publish('close_popup');
        });
    });
}


export function initMarkers() {
    loadMarkers().then(result => {
        result.map(marker => publish('add_marker', marker))
    })
}
