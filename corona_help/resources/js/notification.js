function openNotification(selector, message) {
    let container = document.querySelector(selector);
    container.classList.remove('d-none');
    container.classList.add('d-block');
    let content = container.querySelector('.content');
    content.innerHTML = message


    setTimeout(() => {
        container.classList.remove('d-block');
        container.classList.add('d-none');
    }, 4000)
}


export function openSuccessNotification(message) {
    openNotification("#notificationBoxSuccess", message);
}

export function openErrorNotification(message) {
    openNotification("#notificationBoxError", message);
}
