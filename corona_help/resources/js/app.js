import {
    addUserPanel,
    createLogInModal, createMap, createMessaging,
    createSignInModal,
} from "./template";

import {bootstrap, initSse} from "./bootstrap";

function init() {
    window.corona = {};

    return new Promise(resolve => {

        // document.querySelector('.icon-envelop').addEventListener('click', function () {
        //     toggleMessaging()
        // });

        resolve();

    })

}

document.addEventListener("DOMContentLoaded", async function() {

    await init();
    createSignInModal();
    createLogInModal();
    createMessaging();
    createMap();
    addUserPanel();
    bootstrap().then(() => {
        initSse();
    })

});


