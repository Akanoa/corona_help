export function safeRemoveClass(element, className="corona-hide") {
    if (element.classList.contains(className)) {
        element.classList.remove(className)
    }
}

export function safeAddClass(element, className="corona-hide") {
    if (!element.classList.contains(className)) {
        element.classList.add(className)
    }
}

export function toggleClass(element, className = "corona-hide") {
    if (element.classList.contains(className)) {
        element.classList.remove(className)
    } else {
        element.classList.add(className)
    }
}

export function safeRemoveAttribute(element, attributeName) {
    let attribute = element.getAttribute(attributeName);
    if (attribute !== null || attribute !== "") {
        element.removeAttribute(attributeName)
    }
}

export function safeAddAttribute(element, attributeName, value="") {
    let attribute = element.getAttribute(attributeName);
    if (attribute === null || attribute === "") {
        element.setAttribute(attributeName, "")
    }
}

export function stringToHTML(str) {

    let parser = new DOMParser();
    let html = parser.parseFromString(str, 'text/html');
    return html.querySelector('body').firstChild;
}
