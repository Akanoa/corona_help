

export const CAN_HELP = 'can_give_help';
export const NEED_HELP = 'need_help';

let availableClusters = {}
availableClusters[CAN_HELP] = {
    'className' : 'cluster-need',
    'controlLabel' : "Besoin d'aide",
    'icon' : {
        iconUrl: `/images/vendor/leaflet/dist/need.png`,
        iconSize: [45, 45],
        iconAnchor: [0, 0],
        popupAnchor: [0, 0],
    }
};

availableClusters[NEED_HELP] = {
    'className' : 'cluster-help',
    'controlLabel' : "Peut aider",
    'icon' : {
        iconUrl: `/images/vendor/leaflet/dist/help.png`,
        iconSize: [45, 45],
        iconAnchor: [0, 0],
        popupAnchor: [0, 0],
    }
};

export const AVAILABLE_CLUSTERS = availableClusters;

export const configuration = [
    {
        label : "Je peux aider",
        value: CAN_HELP,
        options: [
            {
                label: "Faire les courses pour une autre personne",
                value: "errants"
            },
            {
                label: "Tenir compagnie à une personne isolée",
                value: "keep_company"
            },
            {
                label: "Garder les enfants des autres",
                value: "keep_children"
            },
            {
                label: "Garder des animaux ou les promener",
                value: "keep_pets"
            }
        ],
        buttonLabel: "Se proposer",
        heading: "Qu'est ce que je peux faire pour les autres ?"
    },
    {
        label : "J'ai besoin d'aide",
        value: NEED_HELP,
        options: [
            {
                label: "Il faut que l'on fasse mes courses",
                value: "errants"
            },
            {
                label: "J'aimerai que l'on me tienne compagnie",
                value: "keep_company"
            },
            {
                label: "Il faudrait que l'on garde mes enfants",
                value: "keep_children"
            },
            {
                label: "Il faudrait que l'on promène ou garde mes animaux",
                value: "keep_pets"
            }
        ],
        buttonLabel: "Demander de l'aide",
        heading: "De quoi ai-je besoin ?"
    }
];

export class Marker {

    constructor({lat, lng, type, details}) {
        this.lat = lat;
        this.lng =  lng;
        this.type = type;
        this.details =  details
    }

    toObject() {
        return {
            lat: this.lat,
            lng: this.lng,
            details: this.details,
            type: this.type
        }
    }
}
