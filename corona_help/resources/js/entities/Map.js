import L from "leaflet";
import 'leaflet.markercluster'
import {AVAILABLE_CLUSTERS, Marker, NEED_HELP} from "./Marker";
import {createMarkerActionPopup, createPopupTemplate} from "../template";
import {createMarkerWithPersistence} from "../marker";

export class Map {

    constructor({lat, lng, zoom, element}) {

        lat = lat || 45.852969;
        lng = lng || 2.349903;
        zoom = zoom ||6;

        this.clusters = [];
        this.markers = [];
        this.currentMarker = null;
        this.map = L.map(element).setView([lat, lng], zoom);
        this.popup = L.popup({
            className: "markerPopup"
        });
        L.tileLayer('https://cartodb-basemaps-{s}.global.ssl.fastly.net/dark_all/{z}/{x}/{y}.png', {
            attribution: 'données © OpenStreetMap/ODbL - rendu OSM France',
            minZoom: 6,
            maxZoom: 19
        }).addTo(this.map);
        this.defineClusters();
        this.defineControls();
        this.registerEvents();
    }

    defineClusters() {

        for (let clusterKey in AVAILABLE_CLUSTERS) {
            let clusterData = AVAILABLE_CLUSTERS[clusterKey];
            // noinspection JSUnresolvedFunction
            let cluster = L.markerClusterGroup({
                iconCreateFunction: function (cluster) {
                    let digits = (cluster.getChildCount() + '').length;
                    return L.divIcon({
                        html: cluster.getChildCount(),
                        className: `cluster ${clusterData['className']} digits-${digits}`,
                        iconSize: null
                    });
                }
            });
            this.clusters[clusterKey] = cluster;
            this.map.addLayer(cluster);
        }
    }

    defineControls() {
        let controls = Object.keys(AVAILABLE_CLUSTERS).reduce((acc, clusterName) => {
            let clusterData = AVAILABLE_CLUSTERS[clusterName];
            acc[clusterData['controlLabel']] = this.clusters[clusterName];
            return acc;
        }, {});

        L.control.layers(null, controls).addTo(this.map)
    }

    persistMarker(data) {
        if (this.currentMarker === null) {
            return;
        }

        let markerData = {...this.currentMarker.toObject(), ...data};
        createMarkerWithPersistence(markerData)
    }

    addMarker({lat, lng, type, uuid, details, creator_uuid, creator_name}) {

        if (Object.keys(AVAILABLE_CLUSTERS).indexOf(type) === -1) {
            return
        }

        let marker = L.marker([lat, lng], {
            icon: L.icon(AVAILABLE_CLUSTERS[type].icon)
        });

        marker.bindPopup(createMarkerActionPopup({uuid, creator_uuid, type, details, creator_name}));

        this.markers[uuid] = {marker, type};
        this.clusters[type].addLayer(marker);
    }

    removeMarker({uuid}) {

        if (!this.markers.hasOwnProperty(uuid)) {
            return
        }

        let {marker, type} = this.markers[uuid];
        this.clusters[type].removeLayer(marker);
        delete this.markers[uuid];
    }

    cleanMarkers() {
        Object.keys(this.markers).map(uuid => this.removeMarker({uuid}))
    }

    invalidateMapSize() {
        this.map.invalidateSize()
    }

    closePopup() {
        this.map.closePopup()
    }


    registerEvents() {
        let self = this;
        this.map.on('click', (e) => {

            // noinspection JSUnresolvedVariable
            let coord = {lat: e.latlng.lat, lng: e.latlng.lng, type: NEED_HELP, details:0};

            self.currentMarker = new Marker(coord);

            // noinspection JSUnresolvedVariable
            self.popup
                .setLatLng(e.latlng)
                .setContent(createPopupTemplate())
                .openOn(this.map)

        })
    }

}
