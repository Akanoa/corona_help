import {openLogInModal} from "./template";
import {getJwtTokenData, initSse} from "./bootstrap";
import Cookies from 'js-cookie'
import {publish} from "./utils/eventBus";

class User {
    constructor() {
        this.name = "";
        this.connected = false;
        this.uuid = "";
        this.conversations = [];
        this.mustLoadConversations = true;
    }

    reset() {
        this.name = "";
        this.connected = false;
        this.uuid = "";
        this.conversations = [];
        this.mustLoadConversations = true;
    }
}

export let currentUser = new User();

export function loginGuard(callback) {

    return new Promise((resolve, reject) => {

        window.corona.loginCallback = () => {
            Promise.resolve(callback())
                .then(result => resolve(result))
                .catch(e => reject(e));
        };

        let cookie = Cookies.get('token');

        if (!cookie || !currentUser.connected) {
            startLoginProcess();
        } else {
            let data = getJwtTokenData(cookie);

            if (data.exp < Math.floor(Date.now() / 1000)) {
                startLoginProcess();
            } else {

                Promise.resolve(callback())
                    .then(result => resolve(result))
                    .catch(e => reject(e));

            }
        }
    });
}

export function startLoginProcess() {
    openLogInModal();
}

export function disconnect() {
    Cookies.remove('token');
    publish('disconnectUser');
    publish('clean_conversations');
    currentUser.reset();
    initSse();
}
