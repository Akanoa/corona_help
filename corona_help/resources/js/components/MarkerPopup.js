import {safeAddAttribute, safeRemoveAttribute, stringToHTML, toggleClass} from "../utils/element";
import {removeMarker} from "../template";
import {CAN_HELP} from "../entities/Marker";
import {updateMarker} from "../api/marker";
import {publish} from "../utils/eventBus";

let template  = `
<style>

.hide {
    display: none;
}
#choices {
    padding: 15px;
}

.popup-container {
    padding-left: 10px !important;
}

.radioAlternative {

    margin-bottom: 10px;
}

#footer {
   display: flex;
   flex-direction: row;
   justify-content: flex-end;
}

#buttonValidate {
    margin-left: 5px;
}

</style>

<!doctype html>
<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    </head>
    <body>
        <div id="root" class="form-check popup-container">
            <div id="choices"></div>
            <div id="footer">
                <button type="button" id="buttonValidate" class="btn btn-primary" disabled>toot</button>
            </div>
        </div>
    </body>
</html>



`;

function createRadioAlternative({label, value, type, heading}) {
    let checked = type === value;
    return `
    <div data-value=${value} class="radioAlternative">
        <input type="radio" class="form-check-input" name="helpType" value=${value} ${checked ? 'checked' : ''}>
        <label>
            ${label}
        </label>
        <div class="options_container ${checked ?  '': 'hide'}">
            <p class="heading">${heading}</p>
            <div class="options"></div>
        </div>
    </div>
    `
}

function createCheckboxAlternative({label, name, details}, index) {
    let value = Math.pow(2, index);
    let checked = (details & value) === value;
    return `
    <div class="form-check">
        <input type="checkbox" class="form-check-input" name="${name}[]" value=${value} ${checked ? 'checked' : ''}>
        <label>
            ${label}
        </label>
    </div>
    `
}

function isThereAtLeastOnOptionChecked(element) {
    return element
        .querySelectorAll('div input:checked')
        .length > 0;
}

function handleButtonBehavior(button, element) {
    if (isThereAtLeastOnOptionChecked(element)) {
        safeRemoveAttribute(button, 'disabled')
    } else {
        safeAddAttribute(button, 'disabled')
    }
}

class MarkerPopup extends HTMLElement {

    static get observedAttributes() {
        return ['configuration', 'uuid', 'type', 'details']
    }

    constructor() {
        super();
        this._configuration = {};
        this._uuid = "";
        this._details = 0;
        this._type = CAN_HELP;
        this.attachShadow({mode: 'open'});
        this.button = null;
    }

    connectedCallback() {

        this.shadowRoot.innerHTML = template;
        let root = this.shadowRoot.querySelector('#choices');
        let footer = this.shadowRoot.querySelector('#footer');

        this.button = this.shadowRoot.querySelector('#buttonValidate');
        this.button.addEventListener('click', () => this.handleValidateButton());

        if (this.uuid.length) {
            let buttonTemplate = `<button id="buttonDelete" type="button" class="btn btn-danger">Supprimer</button>`;
            let deleteButton = stringToHTML(buttonTemplate);
            deleteButton.addEventListener('click', this.handleRemoveButton.bind(this));
            footer.prepend(deleteButton);
            this.button.innerText = "Modifier";
        }

        for (let configuration of this.configuration) {

            let node = stringToHTML(createRadioAlternative({...configuration, type: this.type}));
            let radio = node.querySelector('input[type="radio"]');
            let optionsDiv =  node.querySelector('.options');

            if (this.uuid.length === 0 && configuration.value === this.type) {
                this.button.innerText = configuration.buttonLabel
            }

            radio.addEventListener('change', () => {

                let alternatives = root.querySelectorAll('.radioAlternative');
                alternatives.forEach((alternative) => {
                    let optionsContainer = alternative.querySelector('.options_container');
                    toggleClass(optionsContainer, 'hide')
                });
                this.button.innerText = configuration.buttonLabel;
                handleButtonBehavior(this.button, radio.parentNode.querySelector('.options'));

            });

            for (let [index, option] of configuration.options.entries()) {
                let alternativeNode = stringToHTML(createCheckboxAlternative({...option, name: configuration.value, details:this.details}, index));
                alternativeNode.addEventListener('change', (event) => {
                    handleButtonBehavior(this.button, event.target.closest('.options'));
                });
                optionsDiv.appendChild(alternativeNode);
            }
            root.appendChild(node);
        }

        if (this.uuid.length) {
            let options = this.shadowRoot.querySelector('[name="helpType"]:checked').parentNode.querySelector('.options');
            handleButtonBehavior(this.button, options)
        }
    }

    handleValidateButton() {
        if (this.uuid.length) {
            this.processUpdateMarker();
        } else {
            this.createMarker()
        }
    }

    handleRemoveButton() {
        if (this.uuid.length) {
            removeMarker(this.uuid)
        }
    }

    getDetailsAsIntAndType() {
        let type = this.shadowRoot.querySelector('[name="helpType"]:checked').value;

        let details =  Array.from(this.shadowRoot.querySelectorAll(`[name="${type}[]"]:checked`))
            ?.map(detail => parseInt(detail.value))
            .reduce((acc, value) => acc + value, 0);

        return {type, details}
    }

    processUpdateMarker() {
        const {type, details} = this.getDetailsAsIntAndType();

        updateMarker(JSON.stringify({
            uuid: this.uuid,
            type,
            details
        }))
    }

    createMarker() {

        const data = this.getDetailsAsIntAndType();

        publish('add_marker_with_persistence', data);
    }

    attributeChangedCallback(attrName, oldValue, newValue) {

        if (newValue !== oldValue) {
            this[attrName] = newValue;
        }
    }

    set configuration(configuration) {
        this._configuration = JSON.parse(atob(configuration));
    }

    get configuration() {
        return this._configuration;
    }

    set uuid(uuid) {
        if (uuid) {
            this._uuid = uuid;
        }
    }

    get uuid() {
        return this._uuid;
    }

    get type() {
        return this._type;
    }

    set type(type) {
        this._type = type;
    }
    get details() {
        return this._details;
    }

    set details(details) {
        this._details = details;
    }

}

customElements.define('marker-popup', MarkerPopup);
