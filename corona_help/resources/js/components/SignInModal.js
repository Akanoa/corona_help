import {closeSignInModal, openLogInModal, toggleLogInModal, toggleSignInModal} from "../template";
import {register} from "../api/auth";
import {openSuccessNotification} from "../notification";

let template = `

<!doctype html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    </head>
    <body>

        <style>

            .hide {
                display: none;
            }

            #root {
                min-width: 400px;
                border-top-right-radius: calc(.3rem - 1px);
                border-top-left-radius: calc(.3rem - 1px);
            }

            .header {
                display: flex;
                align-items: flex-start;
                justify-content: space-between;
                padding: 1rem 1rem;
                border-bottom: 1px solid #dee2e6;
                border-top-right-radius: calc(.3rem - 1px);
                border-top-left-radius: calc(.3rem - 1px);
            }

            #closeButton {
                cursor: pointer;
                font-size: 1.1em;
                line-height: 5px;
            }

            .body {
                padding: 25px;
            }

            .footer {
                border-top: 1px solid #dee2e6;
            }

            .footer div {
                padding: 0.75rem;
            }

        </style>

        <div id="root">
            <div class="header">
                <div class="title">Inscription</div>
                <div id="closeButton">&times;</div>
            </div>
            <div class="body">
                <div class="form-group">
                    <label for="signInName">Nom</label>
                    <input type="text" class="form-control" id="signInName" placeholder="Votre nom">
                    <small data-error="name" class="form-text text-danger hide"></small>
                </div>
                <div class="form-group">
                    <label for="signInEmail">Email</label>
                    <input type="text" class="form-control" id="signInEmail" placeholder="email@example.com">
                    <small data-error="email" class="form-text text-danger hide"></small>
                </div>
                <div class="form-group">
                    <label for="signInPassword">Mot de passe</label>
                    <input type="password" class="form-control" id="signInPassword" placeholder="Mot de passe">
                    <small data-error="password" class="form-text text-danger hide"></small>
                </div>
                <div class="form-group">
                    <label for="signInConfirmationPassword">Confirmation du mot de passe</label>
                    <input type="password" class="form-control" id="signInConfirmationPassword" placeholder="Re-tapez le mot de passe">
                    <small data-error="password_verification" class="form-text text-danger hide"></small>
                </div>
            </div>
            <div class="footer">
                <div class="float-right">
                    <button id="comeBackLoginButton" type="button" class="btn btn-info">Se connecter</button>
                    <button id="performSignInButton" class="btn btn-primary" type="button">S'inscrire</button>
                </div>
            </div>
        </div>
    </body>
</html>
`;

function comeBackLogin() {
    closeSignInModal();
    openLogInModal();
}


class SignInModal extends HTMLElement{
    constructor() {
        super();
        this.attachShadow({mode: "open"})
    }

    connectedCallback() {
        this.shadowRoot.innerHTML = template;
        this.shadowRoot.querySelector('#comeBackLoginButton').addEventListener('click', comeBackLogin);
        this.shadowRoot.querySelector('#performSignInButton').addEventListener('click', this.performSignIn.bind(this));
        this.shadowRoot.querySelector('#closeButton').addEventListener('click', closeSignInModal);
    }

    handleErrors(errors) {

        for (let error in errors) {
            let hintContainer = this.shadowRoot.querySelector(`[data-error=${error}]`);
            hintContainer.classList.remove('hide');
            hintContainer.innerText = errors[error]
            setTimeout(() => {
                hintContainer.classList.add('hide')
            }, 5000);

        }
    }

    performSignIn() {

        let name = this.shadowRoot.querySelector('#signInName').value;
        let email = this.shadowRoot.querySelector('#signInEmail').value;
        let password = this.shadowRoot.querySelector('#signInPassword').value;
        let password_verification = this.shadowRoot.querySelector('#signInConfirmationPassword').value;

        register({name, email, password, password_verification}).then(() => {
            toggleSignInModal();
            openSuccessNotification("Vous êtes bien inscrit")
        }).catch(e => e.json().then((result) => {
            this.handleErrors(result.errors)
        }))
    }
}

customElements.define('signin-modal', SignInModal)
