import {openLogInModal, openSignInModal, toggleMessaging} from "../template";
import {subscribe} from "../utils/eventBus";
import {disconnect} from "../auth";
import {safeAddClass, safeRemoveClass} from "../utils/element";

let template = `

<!doctype html>
    <head>
        <link rel="stylesheet" type="text/css" href="css/_icomoon.css">
    </head>
    <body>

        <style lang="scss">

            :host {
                --button-background: #54BB72;
            }

            .hide {
                display: none !important;
            }

            #root {
                //width: 40%;
                height: 100%;
                display: flex;
                flex-direction: row;
                justify-content: center;
                align-items: center;
            }

            .button {
                width: fit-content;
                padding: 10px;
                height: fit-content;
                background-color: var(--button-background);
                color: white;
                border-radius: 10px;
                margin-left: 5px;
                margin-right: 5px;
                font-size: 1em;
                cursor: pointer;
            }

            .button-icon {
                font-size: 1.75em;
            }

            #userDisconnectedContainer {
                display: flex;
                flex-direction: row;
            }

        </style>

        <div id="root">
            <div class="button button-icon icon-envelop"></div>
            <div class="button button-icon icon-info"></div>
            <div id="userDisconnectedContainer">
                <div class="button" id="registerButton">S'enregistrer</div>
                <div class="button" id="loginButton">Se connecter</div>
            </div>
            <div id="userConnectedContainer" class="hide">
                <div class="button" id="disconnectButton">Se déconnecter</div>
            </div>
        </div>
    </body>
    </html>


`;

class UserPanel extends HTMLElement {

    constructor() {
        super();
        this.attachShadow({mode: "open"})
        this.subscriptions = []
    }


    connectedCallback() {
        let self = this;
        this.shadowRoot.innerHTML = template;
        this.shadowRoot.querySelector('.icon-envelop').addEventListener('click', toggleMessaging);
        this.shadowRoot.querySelector('#registerButton').addEventListener('click', openSignInModal);
        this.shadowRoot.querySelector('#loginButton').addEventListener('click', openLogInModal);
        this.shadowRoot.querySelector('#disconnectButton').addEventListener('click', disconnect);

        this.subscriptions.push(subscribe('userConnected', () => {
            safeAddClass(self.shadowRoot.querySelector('#userDisconnectedContainer'), 'hide')
            safeRemoveClass(self.shadowRoot.querySelector('#userConnectedContainer'), 'hide')
        }));

        this.subscriptions.push(subscribe('disconnectUser', () => {
            safeRemoveClass(self.shadowRoot.querySelector('#userDisconnectedContainer'), 'hide')
            safeAddClass(self.shadowRoot.querySelector('#userConnectedContainer'), 'hide')
        }));

    }

    disconnectedCallback() {
        this.subscriptions.forEach(subscription => subscription.unsubscribe())
    }
}

customElements.define('user-panel', UserPanel);
