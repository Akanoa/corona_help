import './Message'
import {stringToHTML, toggleClass} from "../utils/element";
import {addMessageToConversation} from "../api/conversation";
import {currentUser} from "../auth";
import {publish, subscribe} from "../utils/eventBus";

let template = `

<!doctype html>
    <head>
        <link rel="stylesheet" type="text/css" href="css/_icomoon.css">
    </head>
    <body>
        <style>

            :host {
                --conversation_radius: 5px;
                --color_conversation_footer: #54BB72;
                --color_conversation_footer_textarea: #F6FBF6;
                --color_conversation_sender: #A5C3B2;
                --color_conversation_receiver: #EAF5F4;
                --color_conversation_header: #CCE3DE;
            }

            .corona-hide {
                display: none !important;
            }

            /**
            * Open Mode
            */

            .conversationContainer {
                border-radius: var(--conversation_radius);
                background-color: #fff;
                margin-bottom: 20px;
                box-sizing: border-box;
            }


            /** header */
            .conversationContainer .conversationHeader {
                border-bottom: 1px lightslategrey solid;
                padding: 10px;
                background-color: #A4C3B2;
                color: #fff;
                border-top-left-radius: var(--conversation_radius);
                border-top-right-radius: var(--conversation_radius);
                cursor: pointer;
            }

            /** footer */

            .conversationContainer .conversationFooter {
                border-top: 1px lightslategrey solid;
                display: flex;
                padding: 10px;
                background-color: var(--color_conversation_footer);
                border-bottom-left-radius: var(--conversation_radius);
                border-bottom-right-radius: var(--conversation_radius);

            }

            .conversationContainer .conversationFooter .conversationTextarea {
                width: 80%;
                min-height: 30px;
                background-color: var(--color_conversation_footer_textarea);
                border-radius: 5px;
                padding: 10px;
                box-sizing: border-box;
            }

            .conversationContainer .conversationFooter .sendButtonContainer {
                color: #fff;
                cursor: pointer;
                display: flex;
                margin: auto;

                span {
                    margin: auto;
                    font-size: 1.75em;
                }
            }

            .conversationContainer .messages {
                display: flex;
                flex-direction: column;
            }

            /**
            * Collapsed mode
            */

            .conversationContainerCollapsed {
                border-radius: var(--conversation_radius);
                background-color: #fff;
                margin-bottom: 20px;
                box-sizing: border-box;
                display: flex;
            }

            .conversationInformation {
                display: flex;
                flex-direction: column;
                flex: 1;
            }

            .conversationInformation .conversationHeader {
                border-bottom: 1px lightslategrey solid;
                padding: 10px;
                background-color: #A4C3B2;
                color: #fff;
                border-top-left-radius: var(--conversation_radius);
                cursor: pointer;
            }

            .conversationInformation .conversationFooter {
                text-align: center;
                font-size: 1.2em;
                line-height: 2.4em;
            }


            .conversationControls {
                background-color: var(--color_conversation_footer);
                border-bottom-right-radius: var(--conversation_radius);
                border-top-right-radius: var(--conversation_radius);
                color: #fff;
                cursor: pointer;
                display: flex;
            }

            .openButtonContainer {

                height: 100%;
                display: flex;
                flex-direction: column;
                align-items: center;
            }

            .openButtonContainer span {
                margin: auto;
                font-size: 1.5em;
                padding: 5px;
            }
        </style>

        <div id="root">

        </div>
    </body>
</html>
`;

class Conversation extends HTMLElement {
    get messages() {
        return this._messages;
    }

    set messages(value) {

        this._messages = JSON.parse(atob(value));
    }
    get uuid() {
        return this._uuid;
    }

    set uuid(value) {
        this._uuid = value;
    }
    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value;
    }

    static get observedAttributes() {
        return ['name', 'uuid', 'messages']
    }

    constructor() {
        super();
        this.attachShadow({mode: 'open'});
        this._name = "";
        this._uuid = "";
        this._messages = [];
        this.subscriptions = [];
    }

    connectedCallback() {


        let self = this;

        let conversationTemplate = `
<div>
    <div class="conversationContainer">
        <div  class="conversationHeader">${this.name}</div>
        <div class="messages">

        </div>
        <div class="conversationFooter">
            <div contenteditable="true" class="conversationTextarea"></div>
            <div  class="sendButtonContainer"><span class="icon-paper-plane-o"></span></div>
        </div>
    </div>
    <div class="conversationContainerCollapsed corona-hide">
        <div class="conversationInformation">
            <div class="conversationHeader">${this.name}</div>
            <div class="conversationFooter">Blablabla</div>
        </div>
        <div class="conversationControls">
            <div class="openButtonContainer"><span class="icon-eye"></span></div>
        </div>
    </div>
</div>
`;
        let conversationNode = stringToHTML(conversationTemplate);
        this.messagesNode = conversationNode.querySelector('.messages');

        let conversationContainer = conversationNode.querySelector('.conversationContainer');
        let conversationContainerCollapsed = conversationNode.querySelector('.conversationContainerCollapsed');

        conversationNode.querySelectorAll([".conversationHeader", ".openButtonContainer"]).forEach(element => {
          element.addEventListener('click', function () {

              toggleClass(conversationContainer);
              toggleClass(conversationContainerCollapsed);
              publish('scroll_messages');
          });
        });

        conversationNode.querySelector('.sendButtonContainer').addEventListener('click',  this.addMessage.bind(this))
        conversationNode.querySelector('.conversationTextarea').addEventListener('keydown',  this.handleKeyPress.bind(this))

        this._messages.forEach(function (message) {
            self.addMessageToContainerBox(message)
        });

        let subscribeToNewMessageEvent = subscribe('new_message', payload => {
           let {conversation_uuid, ...message} = payload;
           if (conversation_uuid === this.uuid) {
               self.addMessageToContainerBox(message)
           }
        });
        this.subscriptions.push(subscribeToNewMessageEvent);


        this.shadowRoot.innerHTML = template;
        let root = this.shadowRoot.querySelector('#root');
        root.appendChild(conversationNode)
    }

    addMessageToContainerBox({content, author_uuid, author, date}) {
        let messageTemplate = `<message-box
            content="${content}"
            author-uuid="${author_uuid}"
            author="${author}"
            date="${date}"
            />`;
        let messageNode = stringToHTML(messageTemplate);
        this.messagesNode.appendChild(messageNode);
        publish('scroll_messages');
    }

    handleKeyPress(event) {
        event = event || window.event;
        if (event.keyCode === 13 && !event.shiftKey) {
            event.preventDefault();
            event.stopPropagation();
            this.addMessage();
            return false
        }
    }

    addMessage() {
        let element = this.shadowRoot.querySelector('.conversationTextarea');
        let content = element.innerHTML;

        if (content.trim().length === 0) {
            return
        }

        addMessageToConversation(this.uuid, content, currentUser.uuid).then(({data: {content}}) => {
            this.addMessageToContainerBox({
                content,
                author_uuid:currentUser.uuid,
                author: currentUser.name,
                date: Date.now()/1000
            });
            element.innerHTML = ""
        })
    }

    attributeChangedCallback(attrName, oldValue, newValue) {

        if (newValue !== oldValue) {
            this[attrName] = newValue;
        }
    }

    disconnectedCallback() {
        for (let subscription of this.subscriptions) {
            subscription.unsubscribe()
        }
    }
}

customElements.define('conversation-box', Conversation)
