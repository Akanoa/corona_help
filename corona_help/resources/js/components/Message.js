import {currentUser} from "../auth";
import { formatDistance } from 'date-fns';
import {fr} from 'date-fns/locale';
import {stringToHTML} from "../utils/element";

let template = `

<!doctype html>
    <head>
        <link rel="stylesheet" type="text/css" href="css/_icomoon.css">
    </head>
    <body>
        <style>

            :host {
                --conversation_radius: 5px;
                --color_conversation_footer: #54BB72;
                --color_conversation_footer_textarea: #F6FBF6;
                --color_conversation_sender: #A5C3B2;
                --color_conversation_receiver: #EAF5F4;
                --color_conversation_header: #CCE3DE;
            }

            .corona-hide {
                display: none !important;
            }

            #root {
                display:flex;
                width: 100%;
                flex-direction: column;
            }

            .message {
                padding: 10px;
                border-radius: 5px;
                width: 80%;
                overflow-wrap: break-word;

            }

            .message.--receiver {
                background-color: var(--color_conversation_receiver);
                margin: 10px;
            }

            .message.--sender {
                background-color: var(--color_conversation_sender);
                margin: 10px;
                align-self: flex-end;
            }

            .message p {
                margin-block-start: auto;
                margin-block-end: auto;
            }

            .header .author{
                font-weight: bold;
            }

            .footer {
                margin-top: 10px;
                display: flex;
                flex-direction: column;
                padding-top: 5px;
            }

            .footer .date {
                align-self: flex-end;
                font-style: italic;
            }

        </style>

        <div id="root">

        </div>
    </body>
</html>
`;

class Message extends HTMLElement{
    get date() {
        return this._date;
    }

    set date(value) {
        this._date = value;
    }
    get authorUuid() {
        return this._authorUuid;
    }

    set authorUuid(value) {
        this._authorUuid = value;
    }
    get author() {
        return this._author;
    }

    set author(value) {
        this._author = value;
    }
    get content() {
        return this._content;
    }

    set content(value) {
        this._content = value;
    }

    static get observedAttributes() {
        return ['content', 'author', 'author-uuid', 'date']
    }

    constructor() {
        super();
        this.attachShadow({mode:'open'})
        this._content = "";
        this._author = "";
        this._authorUuid = "";
        this._date = 0;
    }

    connectedCallback() {

        let className = this.authorUuid === currentUser.uuid ? '--sender' : '--receiver';
        let name = this.authorUuid === currentUser.uuid ? 'Moi' : this.author;
        let dateAsString = formatDistance(new Date(this.date*1000), new Date(), {locale: fr, includeSeconds:true, addSuffix: true});

        let messageTemplate = `
<div class="message ${className}">
    <div class="header">
        <span class="author">${name}:</span>
    </div>
    <span>${this.content}</span>
    <div class="footer">
        <span class="date">${dateAsString}</span>
    </div>
</div>
`;
        this.shadowRoot.innerHTML = template;
        let messageNode = stringToHTML(messageTemplate)

        this.shadowRoot.querySelector('#root').appendChild(messageNode)
    }

    attributeChangedCallback(attrName, oldValue, newValue) {

        attrName = attrName.replace(/\b-([a-z])/g, (_, char) => char.toUpperCase());

        if (newValue !== oldValue) {
            this[attrName] = newValue;
        }
    }
}

customElements.define('message-box', Message);

