import {createConversationFromMarker} from "../api/conversation";
import {currentUser, loginGuard} from "../auth";
import {publish} from "../utils/eventBus";

let template = `<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <style>
            .labelText {
                font-size: 1rem;
                line-height: 1.5;
            }

            .footer {
                display: flex;
                flex-direction: column;
                align-items: flex-end;
            }

            #pseudo {
                font-weight: bold;
            }
        </style>
    </head>
    <body>
        <div class=form-check">

            <div class="form-group">
                <label class="labelText" for="name">Nom de la conversation</label>
                <input id="name" type="text" class="form-control" placeholder="ex: Course de Rolande">
            </div>

            <div class="form-group">
                <label class="labelText" for="content">Commencer une conversation avec <span id="pseudo"></span></label>
                <textarea id="content" class="form-control" rows="3" placeholder="Votre message"></textarea>
            </div>

            <div class="footer">
                <button id="sendButton" type="button" class="btn btn-primary">Envoyer</button>
            </div>

        </div>
    </body>
</html>`;

function startNewConversationFromMarker({uuid, name, content}) {

    let callback = function() {
        createConversationFromMarker(uuid, content, name)
            .then((response) => {

                let {conversation_uuid: uuid, content} = response.data;

                let messages = [
                    {
                        content,
                        author_uuid: currentUser.uuid,
                        author: currentUser.name,
                        date: Date.now()/1000
                    }
                ];

                publish('new_conversation', {messages, name, uuid})


            }).catch((error) => {
            error.json().then(e => {
                //openErrorNotification(e.error)
            })
        })
            .finally(() => {
                publish('close_popup')
            });
    };

    loginGuard(callback);
}

class NewConversation extends HTMLElement {
    get uuid() {
        return this._uuid;
    }

    set uuid(value) {
        this._uuid = value;
    }
    get creatorName() {
        return this._creatorName;
    }

    set creatorName(value) {
        this._creatorName = value;
    }
    get creatorUuid() {
        return this._creatorUuid;
    }

    set creatorUuid(value) {
        this._creatorUuid = value;
    }
    constructor() {
        super();
        this.attachShadow({mode: 'open'});
        this._creatorUuid = "";
        this._creatorName = "";
        this._uuid = "";
    }

    static get observedAttributes() {
        return ['creator-uuid', 'creator-name', 'uuid']
    }

    connectedCallback() {
        this.shadowRoot.innerHTML = template;
        let pseudo = this.shadowRoot.querySelector('#pseudo');
        pseudo.innerText = this.creatorName;

        this.shadowRoot.querySelector('#sendButton').addEventListener('click', this.createNewConversation.bind(this))
    }

    createNewConversation() {

        let content = this.shadowRoot.querySelector('#content').value;
        let name = this.shadowRoot.querySelector('#name').value;

        startNewConversationFromMarker({
            uuid: this.uuid,
            name,
            content
        })
    }

    attributeChangedCallback(attrName, oldValue, newValue) {

        attrName = attrName.replace(/\b-([a-z])/g, (_, char) => char.toUpperCase());

        if (newValue !== oldValue) {
            this[attrName] = newValue;
        }
    }
}

customElements.define('new-conversation-popup', NewConversation);
