import {
    closeLogInModal,
    openSignInModal,
} from "../template";
import {login} from "../api/auth";
import {bootstrap} from "../bootstrap";

let template = `
<!doctype html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    </head>
    <body>

        <style>

            .hide {
                display: none;
            }

            #root {
                min-width: 400px;
                border-top-right-radius: calc(.3rem - 1px);
                border-top-left-radius: calc(.3rem - 1px);
            }

            .header {
                display: flex;
                align-items: flex-start;
                justify-content: space-between;
                padding: 1rem 1rem;
                border-bottom: 1px solid #dee2e6;
                border-top-right-radius: calc(.3rem - 1px);
                border-top-left-radius: calc(.3rem - 1px);
            }

            #closeButton {
                cursor: pointer;
                font-size: 1.1em;
                line-height: 5px;
            }

            .body {
                padding: 25px;
            }

            .footer {
                border-top: 1px solid #dee2e6;
            }

            .footer div {
                padding: 0.75rem;
            }

        </style>

        <div id="root">
            <div class="header">
                <div class="title">Connexion</div>
                <div id="closeButton">&times;</div>
            </div>
            <div class="body">
                <div class="form-group">
                    <label for="loginEmail">Email</label>
                    <input type="text" class="form-control" id="loginEmail" placeholder="email@example.com">
                    <small data-error="email" class="form-text text-danger hide"></small>
                </div>
                <div class="form-group">
                    <label for="loginPassword">Mot de passe</label>
                    <input type="password" class="form-control" id="loginPassword" placeholder="Mot de passe">
                    <small data-error="password" class="form-text text-danger hide"></small>
                </div>
            </div>
            <div class="footer">
                <div class="float-right">
                    <button id="startRegistrationButton" type="button" class="btn btn-info">S'inscrire</button>
                    <button id="performLogInButton" class="btn btn-primary" type="button">Se connecter</button>
                </div>
            </div>
        </div>
    </body>
</html>
`;

export function startRegistration() {
    closeLogInModal();
    openSignInModal();
}

class LoginModal extends  HTMLElement {
    constructor() {
        super();
        this.attachShadow({mode: "open"})
    }

    connectedCallback() {

        this.shadowRoot.innerHTML = template;
        this.shadowRoot.querySelector('#startRegistrationButton').addEventListener('click', startRegistration);
        this.shadowRoot.querySelector('#performLogInButton').addEventListener('click', this.performLoginIn.bind(this));
        this.shadowRoot.querySelector('#closeButton').addEventListener('click', closeLogInModal);
    }

    performLoginIn() {
        let password = this.shadowRoot.querySelector('#loginPassword').value;
        let email = this.shadowRoot.querySelector('#loginEmail').value;

        login(email, password).then(() => {
            bootstrap().then(() => {
                if (window.corona.loginCallback && typeof window.corona.loginCallback === 'function') {
                    window.corona.loginCallback()
                }
            });
            closeLogInModal();
        }).catch(e => e.json().then((result) => {
            this.handleErrors(result.errors)
        }))
    }

    handleErrors(errors) {

        for (let error in errors) {
            let hintContainer = this.shadowRoot.querySelector(`[data-error=${error}]`);
            hintContainer.classList.remove('hide');
            hintContainer.innerText = errors[error]
            setTimeout(() => {
                hintContainer.classList.add('hide')
            }, 5000);

        }
    }
}

customElements.define('login-modal', LoginModal);
