import {Map} from '../entities/Map.js'
import {subscribe} from "../utils/eventBus";

let template = `
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="css/_leaflet.css">
        <style>

        :host {
            --color_need: #CE4B31;
            --color_help: #4FBB71;
        }

        .cluster {
            background: #3498db;
            border-radius: 50%;
            text-align: center;
            color: white;
            font-weight: 700;
            border: 1px solid #3498db;
            font-family: monospace;

        }

        .cluster-help {
            background: var(--color_help);
            border: 1px solid var(--color_help);
        }

        .cluster-need {
            background: var(--color_need);
            border: 1px solid var(--color_need);
        }


        .cluster:before {
            content: ' ';
            position: absolute;
            border-radius: 50%;
            z-index: -1;
            top: 1px;
            left: 1px;
            right: 1px;
            bottom: 1px;
            border: 1px solid white;
        }
        .digits-1 {
            font-size: 14px;
            height: 28px;
            width: 28px;
            line-height: 28px;
            margin-top: -14px;
            margin-left: -14px;
        }
        .digits-2 {
            font-size: 16px;
            height: 34px;
            width: 34px;
            line-height: 35px;
            margin-top: -17px;
            margin-left: -17px;
        }
        .digits-2:before {
            border-width: 2px;
        }
        .digits-3 {
            font-size: 18px;
            height: 48px;
            width: 47px;
            line-height: 47px;
            border-width: 3px;
            margin-top: -24px;
            margin-left: -24px;
        }
        .digits-3:before {
            border-width: 3px;
        }
        .digits-4 {
            font-size: 18px;
            height: 58px;
            width: 58px;
            line-height: 57px;
            border-width: 4px;
            margin-top: -29px;
            margin-left: -29px;
        }
        .digits-4:before {
            border-width: 4px;
        }


            #mapContainer {

                width: 100%;
                padding: 15px;
                box-sizing: border-box;
                height: 100%;
                display: flex;
            }

            #map {
                box-sizing: border-box;
                max-height: 100%;
                flex: 1;
            }
        </style>
    </head>
    <body>
        <div id="mapContainer">
            <div id="map"></div>
        </div>
    </body>
</html>
`;


class MapBox extends HTMLElement {

    constructor() {
        super();
        this.attachShadow({mode: 'open'});
        this.subscriptions = [];
        this.map = null;
    }

    connectedCallback() {
        let self = this;
        this.shadowRoot.innerHTML = template;
        let element = this.shadowRoot.querySelector('#map');
        this.map = new Map({element});

        let subscriptionClosePopup = subscribe('close_popup', function () {
            self.map.closePopup();
        });
        this.subscriptions.push(subscriptionClosePopup);

        let subscriptionAddMarker = subscribe('add_marker', marker => {
            this.map.addMarker(marker)
        });
        this.subscriptions.push(subscriptionAddMarker);

        let subscriptionRemoveMarker = subscribe('remove_marker', payload => {
            this.map.removeMarker(payload)
        });
        this.subscriptions.push(subscriptionRemoveMarker);

        let subscriptionCleanMarkers = subscribe('clean_markers', () => {
            this.map.cleanMarkers()
        });
        this.subscriptions.push(subscriptionCleanMarkers);

        let subscriptionInvalidateMapSize = subscribe('invalidate_map_size', () => {
            this.map.invalidateMapSize()
        });
        this.subscriptions.push(subscriptionInvalidateMapSize);

        let subscriptionAddMarkerWithPersistence = subscribe('add_marker_with_persistence', marker => {
            self.map.persistMarker(marker)
        });
        this.subscriptions.push(subscriptionAddMarkerWithPersistence);

    }

    disconnectedCallback() {
        for (let subscription of this.subscriptions) {
            subscription.unsubscribe();
        }
    }
}

customElements.define('map-box', MapBox);
