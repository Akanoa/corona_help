import {publish, subscribe} from "../utils/eventBus";
import './Conversation';
import {stringToHTML} from "../utils/element";

let template = `
<html>
    <head>
        <style>
            #messagesContainer {

                overflow-y: auto;
                padding: 15px;
                box-sizing: border-box;
                order: 2;
                background-color: #CCE5DF;
                border-radius: 30px;
                width: 100%;
                height: 100%;
            }

            #messagesHeader {
                text-align: center;
                padding: 10px;
                margin-bottom: 10px;

            }

            #messagesContainerBox {

                padding: 10px;
                margin-bottom: 5px;
                display: flex;
                flex-direction: column;

            }

        </style>
    </head>
    <body>
    <div id="messagesContainer">
        <div id="messagesHeader">
            Messagerie
        </div>
        <div id="messagesContainerBox"></div>
    </div>
    </body>
</html>
`;

class Messaging extends HTMLElement {

    constructor() {
        super();
        this.attachShadow({mode: 'open'});
        this.subscriptions = [];
    }

    connectedCallback() {
        this.shadowRoot.innerHTML = template;
        this.conversationNode = this.shadowRoot.querySelector('#messagesContainerBox');
        let self = this;

        let newConversationSubscription = subscribe('new_conversation', function (conversation) {
            self.addConversation(conversation)
        });
        this.subscriptions.push(newConversationSubscription);

        let cleanSubscription = subscribe('clean_conversations', function () {
            self.shadowRoot.querySelector("#messagesContainerBox").innerHTML = '';
        });
        this.subscriptions.push(cleanSubscription);

        let scrollSubscription = subscribe('scroll_messages', function () {
            let element = self.shadowRoot.querySelector("#messagesContainer");
            element.scrollTo(0, element.scrollHeight);
        });
        this.subscriptions.push(scrollSubscription);
    }

    addConversation({messages, name, uuid}) {

        let template = `
<conversation-box name="${name}" messages=${btoa(JSON.stringify(messages))} uuid="${uuid}"></conversation-box>
`;

        let node = stringToHTML(template);
        this.conversationNode.appendChild(node);
        publish('scroll_messages');
    }

    disconnectedCallback() {
        for (let subscription of this.subscriptions) {
            subscription.unsubscribe()
        }
    }
}

customElements.define('messaging-box', Messaging);
