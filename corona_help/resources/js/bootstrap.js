import Cookies from "js-cookie";
import {publish} from "./utils/eventBus";
import {me} from "./api/auth";
import {currentUser} from "./auth";
import {initMarkers} from "./marker";

export let sse;

export function bootstrap() {

    return new Promise(resolve => {

        initUserData().then(() => {
            publish('clean_markers');
            initMarkers();
            if(currentUser.mustLoadConversations) {

                currentUser.conversations.map(conversation => {
                    publish('new_conversation', conversation)
                });

                currentUser.mustLoadConversations = false;
            }
            resolve()
        });

    });

}

export function initSse() {

    if (sse) {
        sse.close()
    }

    sse = new EventSource('/api/sse', {withCredentials: true})

    sse.addEventListener('message', e => {
        let data = JSON.parse(e.data);
        publish(data.type, data.payload)
    });

    window.addEventListener('beforeunload', () => {
        sse.close()
    })
}

export function initUserData() {

    return new Promise(resolve => {
        let cookie = Cookies.get('token');
        if (cookie && !currentUser.connected) {
            me().then(response => {

                if (!response.hasOwnProperty('data')) {
                    resolve();
                }

                currentUser.name = response.data.name;
                currentUser.uuid = response.data.uuid;
                currentUser.conversations = response.data.conversations;
                currentUser.connected = true;

                publish('userConnected');
                initSse();
                resolve();

            });
        } else {
            resolve()
        }
    });
}

export function getJwtTokenData(token) {
    let body = token.split('.')[1];
    return JSON.parse(atob(body));
}
