import {configuration} from "./entities/Marker";
import {apiRemoveMarker} from "./api/marker";
import {createConversationFromMarker} from './api/conversation'
import * as Sqrl from 'squirrelly'
import {openErrorNotification} from "./notification";
import {currentUser, loginGuard} from "./auth";

import './components/MarkerPopup';
import './components/UserPanel';
import './components/SignInModal';
import './components/LoginModal';
import './components/Conversation';
import './components/Messaging';
import './components/Map';
import './components/NewConversation.js';

import {safeAddClass, safeRemoveClass, stringToHTML, toggleClass} from "./utils/element";
import {publish} from "./utils/eventBus";

export function removeMarker(uuid) {

    apiRemoveMarker(uuid).then(() => {

        publish('remove_marker', {uuid})

    }).catch(err => {
        console.log(err)
    });

}

export function createMarkerActionPopup({uuid, creator_uuid, creator_name, type, details}) {

    let template = `
<div class="markerContainer">
    <div class="controls">
        {{if(options.sameFingerprint)}}
        <marker-popup
            configuration=${btoa(JSON.stringify(configuration))}
            uuid="${uuid}"
            details="${details}"
            type="${type}"></marker-popup>
        {{#else}}
        <new-conversation-popup
            creator-uuid="${creator_uuid}"
            creator-name="${creator_name}"
            uuid="${uuid}"
        ></new-conversation-popup>
        {{/if}}
    </div>
</div>

`;

    return Sqrl.Render(template, {
        uuid,
        sameFingerprint: creator_uuid === currentUser.uuid
    });

}


export function createPopupTemplate() {

    return `<marker-popup configuration=${btoa(JSON.stringify(configuration))} ></marker-popup>`;
}

export function addUserPanel() {
    let template = `<user-panel></user-panel>`;
    let node = stringToHTML(template);
    let helpers = document.querySelector('#headerContainer')
    helpers.appendChild(node)
}

export function createMessaging() {
    let template = `<messaging-box id="messagesContainer"></messaging-box>`;

    let node = stringToHTML(template);
    let container = document.querySelector('#contentContainer');
    container.appendChild(node);

}

export function createMap() {
    let template = `<map-box id="mapContainer"></map-box>`;

    let node = stringToHTML(template);
    let container = document.querySelector('#mapContainer');
    container.appendChild(node);

}

export function toggleMessaging() {
    let container = document.querySelector('#messagesContainer');
    toggleClass(container, 'corona-hide');
    publish('invalidate_map_size')
}

export function toggleSignInModal() {
    let container = document.querySelector('#signInModal');
    toggleClass(container, 'corona-hide')
}

// sign in related
export function openSignInModal() {
    let container = document.querySelector('#signInModal');
    safeRemoveClass(container)
}

export function closeSignInModal() {
    let container = document.querySelector('#signInModal');
    safeAddClass(container)
}
export function createSignInModal() {

    let template = `<signin-modal class="corona-hide" id="signInModal"></signin-modal>`;

    let node = stringToHTML(template);
    let container = document.querySelector('#mainContainer');
    container.appendChild(node);
}

// login related
export function openLogInModal() {
    let container = document.querySelector('#logInModal');
    safeRemoveClass(container)
}

export function closeLogInModal() {
    let container = document.querySelector('#logInModal');
    safeAddClass(container)
}


export function createLogInModal() {

    let template = `<login-modal class="corona-hide" id="logInModal"></login-modal>`;

    let node = stringToHTML(template);
    let container = document.querySelector('#mainContainer');
    container.appendChild(node);
}
