<?php

namespace App;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Message
 *
 * @property int $id
 * @property string $conversation_uuid
 * @property string $content
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Message newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Message newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Message query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Message whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Message whereConversationUuid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Message whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Message whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Message whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $user_uuid
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Message whereUserUuid($value)
 */
class Message extends Model implements ToDocumentInterface
{
    protected $fillable = ['content', 'conversation_uuid'];

    /**
     * @return User|\Illuminate\Database\Eloquent\Relations\BelongsTo|object
     *
     */
    public function user() {

        return $this->belongsTo(User::class, 'user_uuid')->first();
    }

    /**
     * @return array
     */
    public function toDocument() : array {

        $author = $this->user();
        return [
            'author' => $author->name,
            'author_uuid' => $author->uuid,
            'content' => $this->content,
            'date' => $this->created_at->timestamp,
            'conversation_uuid' => $this->conversation_uuid
        ];
    }
}
