<?php

namespace App;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Types\Type;
use Grimzy\LaravelMysqlSpatial\Doctrine\Point;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * App\Marker
 *
 * @property string $uuid
 * @property string $creator_uuid
 * @property \Grimzy\LaravelMysqlSpatial\Types\Point $coordinates
 * @property int $details
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Marker newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Marker newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Marker query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Marker whereCoordinates($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Marker whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Marker whereCreatorUuid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Marker whereDetails($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Marker whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Marker whereUuid($value)
 * @mixin \Eloquent
 * @property string $type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Marker whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Marker comparison($geometryColumn, $geometry, $relationship)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Marker contains($geometryColumn, $geometry)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Marker crosses($geometryColumn, $geometry)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Marker disjoint($geometryColumn, $geometry)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Marker distance($geometryColumn, $geometry, $distance)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Marker distanceExcludingSelf($geometryColumn, $geometry, $distance)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Marker distanceSphere($geometryColumn, $geometry, $distance)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Marker distanceSphereExcludingSelf($geometryColumn, $geometry, $distance)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Marker distanceSphereValue($geometryColumn, $geometry)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Marker distanceValue($geometryColumn, $geometry)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Marker doesTouch($geometryColumn, $geometry)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Marker equals($geometryColumn, $geometry)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Marker intersects($geometryColumn, $geometry)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Marker orderByDistance($geometryColumn, $geometry, $direction = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Marker orderByDistanceSphere($geometryColumn, $geometry, $direction = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Marker orderBySpatial($geometryColumn, $geometry, $orderFunction, $direction = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Marker overlaps($geometryColumn, $geometry)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Marker within($geometryColumn, $polygon)
 * @property-read \App\User $user
 */
class Marker extends Model implements ToDocumentInterface
{
    use SpatialTrait;

    public function __construct()
    {
        try {
            Type::addType('my_point', Point::class);
        } catch (DBALException $e) {

        }
        DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('point', 'my_point');
    }

    protected $table = 'markers';
    protected $primaryKey = 'uuid';
    protected $keyType = 'string';
    public $incrementing = false;
    protected $spatialFields = [
        'coordinates'
    ];

    public function user() {
        return $this->hasOne(User::class, 'uuid', 'creator_uuid');
    }

    public function broadcast($type) {

            (new \Filld\Amqp\Amqp)->publish('broadcast', json_encode([
                'payload' => $this->toDocument(),
                'type' => $type
            ]), [
                'exchange' => 'sse'
            ]);
    }

    public function toDocument(): array
    {
        return [
            'uuid' => $this->uuid,
            'creator_uuid' => $this->creator_uuid,
            'creator_name' => $this->user->name,
            'type' => $this->type,
            'lat' => $this->coordinates->getLat(),
            'lng' => $this->coordinates->getLng(),
            'details' => $this->details
        ];
    }
}
