<?php

namespace App\Http\Controllers;

use Filld\Amqp\Amqp;
use Filld\Amqp\Consumer;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Hhxsv5\SSE\SSE;
use Hhxsv5\SSE\Update;

class EventController extends Controller
{
    public function sse() {


        if (auth('api')->user()) {
            $queueName = sprintf('user-%s', auth('api')->user()->getAuthIdentifier());
            $routing = [
                'broadcast',
                sprintf('sse-%s', auth('api')->user()->getAuthIdentifier())
            ];
        } else {
            $queueName = sprintf('anomymous-%s', Uuid::uuid4()->toString());
            $routing = [
              'broadcast'
            ];
        }

        $response = new StreamedResponse();
        $response->setCallback(function () use ($queueName, $routing) {

            (new SSE)->start(new Update(function () use($queueName, $routing) {

                (new Amqp)->consume($queueName, function ($message, Consumer $resolver) use($queueName, $routing) {

                    $data = json_decode($message->body, true);
                    echo sprintf("data: %s\n\n", json_encode([
                        'payload' => $data['payload'],
                        'type' => $data['type']
                    ]));
                    ob_flush();
                    flush();
                    $resolver->acknowledge($message);
                    $resolver->stopWhenProcessed();
                },
                    [
                        'queue_auto_delete' => true,
                        'queue_properties' => ['x-expires' => ['I', 10000]],
                        'exchanges' => [
                            [
                                'exchange' => 'sse',
                                'exchange_type' => 'direct',
                                'exchange_passive'      => false,
                                'exchange_durable'      => true,
                                'exchange_auto_delete'  => false,
                                'exchange_internal'     => false,
                                'exchange_nowait'       => false,
                                'exchange_properties'   => [],
                                'routing' => $routing
                            ]
                        ]
                    ]
                );
            }));


        });
        $response->headers->set('Content-type', 'text/event-stream');
        $response->headers->set('X-Accel-Buffering', 'no');
        $response->headers->set('Cache-Control', 'no-cache');
        $response->send();
    }
}
