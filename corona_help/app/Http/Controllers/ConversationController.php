<?php

namespace App\Http\Controllers;

use App\Conversation;
use App\Marker;
use App\Message;
use App\User;
use Filld\Amqp\Amqp;
use Ramsey\Uuid\Uuid;

class ConversationController extends Controller
{

    public function create() {
        $markerUuid = \request('marker_uuid');

        /** @var Marker $marker */
        $marker = Marker::whereUuid($markerUuid)->first();

        if (is_null($marker)) {
            return response()->json(['error' => "Vous ne pouvez pas créer de conversation sur un marqueur inexistant"], 400);
        }

        $conversation = new Conversation();
        $conversationUuid = Uuid::uuid4()->toString();
        $conversation->uuid = $conversationUuid;
        $conversation->name = \request('name');
        $conversation->marker_uuid = $markerUuid;

        $conversation->save();

        $content = clean(\request('content'));
        $userUuid = \request('user_uuid');

        $message = new Message();
        $message->content = $content;
        $message->conversation_uuid = $conversation->uuid;
        $message->user_uuid = $userUuid;
        $message->save();

        $conversation->users()->attach($marker->creator_uuid);
        $conversation->users()->attach($userUuid);

        $conversation->notifyConversationCreationToUsers($conversation, $userUuid);


        return response()->json(['data' => [
            'conversation_uuid' => $conversation->uuid,
            'content' => $content
        ]]);



    }

    public function addMessage() {

        $conversationUuid = \request('uuid');

        $conversation = Conversation::whereUuid($conversationUuid)->first();

        if (!$conversation) {
            return response()->json(['error' => "Cette conversation n'existe pas"], 400);
        }

        $content = clean(\request('content'));

        $message = new Message();
        $message->content = $content;
        $message->conversation_uuid = $conversationUuid;
        $message->user_uuid = \request('author');
        $message->save();

        $conversation->notifyMessageCreationToUsers($message);

        return response()->json(['message' => "Le message a bien été envoyé", 'data' => [
            'content' => $content
        ]]);
    }
}
