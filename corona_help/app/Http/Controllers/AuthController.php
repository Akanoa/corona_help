<?php

namespace App\Http\Controllers;

use App\Conversation;
use App\Mail\EmailVerification;
use App\Message;
use App\User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register', 'confirmation']]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function register()
    {

        $messages = [
            'name.required' => "Le nom de l'utilisateur est requis",
            'email.required' => "L'email est requis",
            'password.required' => "Le mot de passe est requis",
            'password_verification.required' => "La vérification du mot de passe est requise",
            'password_verification.same' => "Les deux mot de passe doivent correspondre",
            'email.email' => "L'email doit est valide",
            'email.unique' => "Cet mail est déjà utilisé par quelqu'un d'autre"
        ];

        $validator = Validator::make(\request()->all(), [
            'name' => 'required|max:255',
            'email' => 'required|email:rfc|unique:users',
            'password' => 'required',
            'password_verification' => 'required|same:password'
        ], $messages);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->toArray()], 401);
        }

        $user = new User();
        $user->fill([
           'uuid' => Uuid::uuid4()->toString(),
           'email' => request('email'),
           'name' => request('name'),
           'password' => Hash::make(request('password')),
           'token' => Uuid::uuid4()->toString()
        ]);

        $user->save();

        $message = new EmailVerification($user->token);
        Mail::to($user->email)->queue($message);

        return \response()->json(['message' => 'User registered with email: '.request('email')]);
    }

    public function login()
    {

        $messages = [
            'email.required' => "L'email est requis",
            'password.required' => "Le mot de passe est requis",
        ];

        $validator = Validator::make(\request()->all(), [
            'email' => 'required',
            'password' => 'required',
        ], $messages);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->toArray()], 401);
        }

        $credentials = \request(['email', 'password']);

        if (! $token = auth('api')->attempt($credentials)) {
            return response()->json(['errors' => ['password' => "Le mot de passe ou l'email est invalide"]], 401);
        }

        if (!auth('api')->user()->verified) {
            return \response()->json(['errors' => ['email' => "Veuillez confirmer votre adresse mail"]], 401);
        }

        return $this->responseWithToken($token);
    }

    public function confirmation($token) {

        /** @var User $user */
        $user = User::where('token', $token)->first();
        if ($user === null) {
            return view('mail.email_failure_verification');
        }

        $user->verified = true;
        $user->token = null;

        $user->save();

        return view('mail.email_success_verification');
    }

    public function me() {

        $authUser = auth('api')->user();
        $userUuid = $authUser->getAuthIdentifier();

        $user = User::whereUuid($userUuid)->first();

        if (!$user) {
            return \response()->json(['error' => "Impossible de retrouver l'utilisateur demandé"], 400);
        }

        $conversations = $user->conversations()
            ->get()
            ->map(function (Conversation $conversation) {
                return $conversation->toDocument();
            });

        $data = [
            'uuid' => $user->uuid,
            'name' => $user->name,
            'conversations' => $conversations
        ];

        return \response()->json(['data' => $data]);
    }

    public function logout()
    {
        auth('api')->logout();

        return response()->json(['message' => "Successfully logged out"]);
    }

    protected function responseWithToken($token)

    {
        $response = new Response();
        $response->withCookie('token', $token, 60*60*24, '/', '', false, false);
        return $response;

    }
}
