<?php

namespace App\Http\Controllers;

use App\Marker;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class MarkerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        $markers = Marker::all();
        $data = array_map(function ($marker){
            /** @var Marker $marker */
            return $marker->toDocument();
        }, $markers->all());

        return \response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return JsonResponse
     */
    public function create()
    {

        $creatorUserUuid = \request('creator_uuid');

        //$markers = Marker::whereCreatorUuid($creatorUserUuid)->get();

//        if ($markers->count() > 1) {
//            return \response()->json(['error' => "Le nombre de marqueurs par personne est limité à 2"], 401);
//        }

        $uuid = Uuid::uuid4()->toString();
        $marker = new Marker();
        $marker->uuid = $uuid;
        $marker->creator_uuid = $creatorUserUuid;
        $marker->type = \request('type');
        $marker->coordinates = new Point(\request('lat'), \request('lng'));
        $marker->details = \request('details');

        $marker->save();

        $marker->uuid = $uuid;
        $marker->broadcast('add_marker');

        return \response()->json(['data' => $marker->toDocument()]);
    }

    /**
     * @return JsonResponse
     */
    public function update() {

        $marker = Marker::whereUuid(\request('uuid'))->first();

        if (is_null($marker)) {
            return \response()->json(['error' => "Le marqueur n'existe pas"], 400);
        }

        $marker->type = \request('type');
        $marker->details = \request('details');
        $marker->save();

        return \response()->json(['message' => "Le marqueur a bien été mis à jour"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $uuid
     * @return JsonResponse
     */
    public function destroy($uuid)
    {
        $userUuid = auth('api')->user()->getAuthIdentifier();

        $marker = Marker::whereCreatorUuid($userUuid)
            ->whereUuid($uuid)
            ->first();

        if ($marker) {
            Marker::destroy($marker->uuid);
            $marker->broadcast('remove_marker');
            return \response()->json(['message' => "Le marqueur a bien été supprimé"]);

        } else {
            return \response()->json(['error' => 'Ce marqueur appartient à un autre utilisateur'], 401);
        }

    }
}
