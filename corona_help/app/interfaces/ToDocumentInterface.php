<?php


namespace App;


interface ToDocumentInterface
{
    public function toDocument() : array ;
}
