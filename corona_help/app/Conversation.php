<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Conversation
 *
 * @property string $uuid
 * @property string $name
 * @property string $marker_uuid
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Message[] $messages
 * @property-read int|null $messages_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Conversation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Conversation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Conversation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Conversation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Conversation whereMarkerUuid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Conversation whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Conversation whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Conversation whereUuid($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @property-read int|null $users_count
 * @property-read \App\Marker $marker
 */
class Conversation extends Model implements ToDocumentInterface
{

    protected $table = 'conversations';
    protected $primaryKey = 'uuid';
    protected $keyType = 'string';
    public $incrementing = false;

    protected $fillable = ['name', 'marker_uuid'];

    public function messages()
    {
        return $this->hasMany(Message::class, 'conversation_uuid');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'pivot_conversations_users')
            ->withPivot('user_uuid', 'conversation_uuid');
    }

    public function marker() {
        return $this->hasOne(Marker::class, 'uuid', 'marker_uuid');
    }

    public function toDocument() : array {

        $messages = $this
            ->messages()
            ->get()
            ->map(function (Message $message) {
                return $message->toDocument();
            });

        return [
            'name' => $this->name,
            'uuid' => $this->uuid,
            'messages' => $messages
        ];
    }

    protected function notifyUser(ToDocumentInterface $entity, $userUuid, $type) {

        $this->users()->get()->each(function (User $user) use($entity, $userUuid, $type) {

            if ($userUuid !== $user->uuid) {
                $queueName = sprintf('sse-%s', $user->uuid);

                (new \Filld\Amqp\Amqp)->publish($queueName, json_encode([
                    'payload' => $entity->toDocument(),
                    'type' => $type
                ]), [
                    'exchange' => 'sse'
                ]);
            }
        });
    }

    public function notifyMessageCreationToUsers(Message $message) {

        $this->notifyUser($message, $message->user()->uuid, 'new_message');
    }

    public function notifyConversationCreationToUsers(Conversation $conversation, $userUuid) {
        $this->notifyUser($conversation, $userUuid, 'new_conversation');
    }

}
